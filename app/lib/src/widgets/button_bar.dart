// ******************************
// Widget boton cerrar sesion
// ******************************

import 'package:app/src/bloc/bloc_user.dart';
import 'package:app/src/widgets/circle_button.dart';
import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

class ButtonsBar extends StatelessWidget {
  // Declaracion de variable para el acceso al BlocUser y por consiguiente al SignOut
  UserBloc userBlock;

  @override
  Widget build(BuildContext context) {
    // instanciar la variable
    userBlock = BlocProvider.of(context);
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
        child: Row(
          children: <Widget>[
            CircleButton(
                true,
                Icons.exit_to_app,
                20.0,
                Color.fromRGBO(255, 255, 255, 0.6),
                () => {
                      // llamar al evento de cerrar sesion
                      userBlock.signOut()
                    }),
          ],
        ));
  }
}
