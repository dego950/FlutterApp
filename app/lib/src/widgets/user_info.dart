// ********************************************
// Widget Mostrar informacion Usuario logueado
// ********************************************

import 'package:app/src/models/user.dart';
import 'package:flutter/material.dart';

class UserInfo extends StatelessWidget {
  // llamado al modelo
  User user;

  UserInfo(@required this.user);

  @override
  Widget build(BuildContext context) {
    final userPhoto = Container(
        width: 90.0,
        height: 90.0,
        margin: EdgeInsets.only(right: 20.0),
        decoration: BoxDecoration(
            border: Border.all(
                color: Colors.white, width: 2.0, style: BorderStyle.solid),
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                    user.photoUrl) // image: AssetImage(user.photoUrl))),
                )));
    final userInfo = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(bottom: 5.0),
            child: Text(user.name,
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue,
                  fontFamily: 'Lato',
                ))),
        Text(user.email,
            style: TextStyle(
                fontSize: 15.0, color: Colors.blue, fontFamily: 'Lato')),
      ],
    );

    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
          child: Row(
            children: <Widget>[userPhoto, userInfo],
          ),
        ),
        Text('Hola Mundo')
      ],
    );
  }
}
