// ******************************
// Home Page
// ******************************

import 'package:app/src/bloc/bloc_user.dart';
import 'package:app/src/pages/profile_trips.dart';
import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserBloc>(
      bloc: UserBloc(),
      child: ProfileTrips(),
    );

    /*
    return Container(
      child: Text('hola mundo'),
    );*/
  }
}
