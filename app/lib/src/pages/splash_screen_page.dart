import 'dart:async';
import 'package:app/src/utils/my_navegator.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), () => MyNavegator.goToHome(context));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text('splash Screen'),
      ),
    );
  }
}
