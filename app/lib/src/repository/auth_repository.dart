// ******************************************************************
// Contiene la lagica de Repository (Controlar las fuentes de datos)
// ******************************************************************

import 'package:app/src/repository/firebase_auth_api.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository {
  final _firebaseAuthAPI = FirebaseAuthAPI();

  Future<User> signInFirebase() => _firebaseAuthAPI.currentUser();

  // fuente de datos para cerrar la sesion
  signOut() => _firebaseAuthAPI.signOut();
}
