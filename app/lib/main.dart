import 'package:app/src/bloc/bloc_user.dart';
import 'package:app/src/pages/home_page.dart';
import 'package:app/src/pages/inicio_sesion.dart';
//import 'package:app/src/pages/inicio_sesion.dart';
import 'package:app/src/pages/splash_screen_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

var routes = <String, WidgetBuilder>{
  "splash": (BuildContext context) => SplashScreen(),
  "login": (BuildContext context) => InicioSesion(),
  "home": (BuildContext context) => Homepage(),
};
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          //home: InicioSesion(),
          initialRoute: 'splash',
          routes: routes,
        ),
        bloc: UserBloc());

    /*
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Container(
            child: Text('Hello World'),
          ),
        ),
      ),
    );*/
  }
}
